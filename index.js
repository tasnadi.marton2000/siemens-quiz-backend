const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const { getQuestions, getQuestionById, postQuestion } = require("./api");

const app = express();
app.use(bodyParser.json());
app.use(cors());

var nrOfQuestions = 50;
var usedQuestions = new Array(nrOfQuestions).fill(0);
var nrOfAnsweredQuestions = 0;

app.post("/game", async (req, resp) => {
  usedQuestions = new Array(50).fill(0);
  nrOfAnsweredQuestions = 0;
  resp.sendStatus(200);
});

app.get("/questions/:id", async (req, res) => {
  const questionId = req.params.id;
  const question = await getQuestionById(questionId);
  if (question == -1) {
    console.log("Error: Get Question by Id.");
  } else {
    console.log(question);
    res.send(question);
  }
});

app.get("/questions", async (req, res) => {
  const questions = await getQuestions();
  if (questions == -1) {
    console.log("Error: Get Questions.");
  } else {
    res.send(questions);
  }
});

app.post("/questions", async (req, resp) => {
  const requestData = req.body;
  const newQuestion = {
    question: requestData.question,
    optionA: requestData.optionA,
    optionB: requestData.optionB,
    optionC: requestData.optionC,
    optionD: requestData.optionD,
    correctAnswer: requestData.correctAnswer,
  };

  await postQuestion(newQuestion);

  resp.json({ message: "Data received successfully", data: requestData });
});

app.get("/randomQuestion", async (req, resp) => {
  const response = await getRandomQuestion();
  if (response == -1) {
    resp.sendStatus(415);
  }
  resp.send(response);
});

async function getRandomQuestion() {
  const min = 0;
  const max = nrOfQuestions;

  if (nrOfAnsweredQuestions == max) {
    return -1;
  }

  let randomInt = Math.floor(Math.random() * (max - min) + min);
  while (usedQuestions[randomInt] != 0) {
    randomInt = Math.floor(Math.random() * (max - min) + min);
  }
  usedQuestions[randomInt] = 1;
  const response = await getQuestionById(randomInt + 1);

  nrOfAnsweredQuestions++;

  return response[0];
}

console.log("The server is listening on port 8082.");
app.listen(8082);
