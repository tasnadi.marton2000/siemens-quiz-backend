const sql = require("mssql");

const config = {
  user: "marci",
  password: "alma",
  server: "localhost",
  database: "quiz",
  port: 1433,
  trustServerCertificate: true,
  options: {
    trustedConnection: true,
  },
};

async function getQuestions() {
  try {
    await sql.connect(config);
    const request = new sql.Request();

    const query = `select * from Questions`;
    const result = await request.query(query);

    console.log("Get questions successfully.");
    console.log(result.recordset);
    return result.recordset;
  } catch (err) {
    console.error(err);
    return -1;
  } finally {
    sql.close();
  }
}

async function getQuestionById(questionId) {
  try {
    await sql.connect(config);
    const request = new sql.Request();

    const query = `select * from Questions WHERE id = ${questionId}`;

    const result = await request.query(query);
    console.log(
      "Get the question with the Id " + questionId + " successfully."
    );
    return result.recordset;
  } catch (err) {
    console.error(err);
    return -1;
  } finally {
    sql.close();
  }
}

async function postQuestion(question) {
  try {
    await sql.connect(config);
    const request = new sql.Request();

    const query =
      "INSERT INTO Questions (question, optionA, optionB, optionC, optionD, correctAnswer) VALUES (@question, @optionA, @optionB, @optionC, @optionD, @correctAnswer)";
    request.input("question", sql.NVarChar, question.question);
    request.input("optionA", sql.NVarChar, question.optionA);
    request.input("optionB", sql.NVarChar, question.optionB);
    request.input("optionC", sql.NVarChar, question.optionC);
    request.input("optionD", sql.NVarChar, question.optionD);
    request.input("correctAnswer", sql.Char, question.correctAnswer);

    const result = await request.query(query);
    console.log("Data inserted successfully");
    return result.recordset;
  } catch (err) {
    console.error(err);
    return -1;
  } finally {
    sql.close();
  }
}

module.exports = { getQuestions, getQuestionById, postQuestion };
